import json

import exifread
import pika
from django.conf import settings

from .models import Gamme, Module, Composant, Composition


class MessageService:

    def __init__(self):
        credentials = pika.PlainCredentials(settings.BROKER_USER, settings.BROKER_PASSWORD)
        parameters = pika.ConnectionParameters(settings.BROKER_HOST,
                                               settings.BROKER_PORT,
                                               settings.BROKER_VHOST,
                                               credentials)
        connection = pika.BlockingConnection(parameters)
        self.channel = connection.channel()
        self.channel.exchange_declare(exchange='madera', exchange_type='topic', durable=True)

    def envoyer_message(self, routing_key, body):
        self.channel.basic_publish(exchange='madera', routing_key=routing_key, body=body)


class ImportService:

    def importer_module(self, fichier):
        tags = exifread.process_file(fichier.file)
        metadatas = json.loads(str(tags['EXIF UserComment']))
        gamme = Gamme.objects.get(uuid=metadatas['gamme'])
        module = Module.objects.create(libelle=metadatas['libelle'], unite=metadatas['unite'], gamme=gamme)
        for metada in metadatas:
            print(f'{metada} : {metadatas[metada]}')

        for composant_associe in metadatas['composants']:
            composant = Composant.objects.get(uuid=composant_associe['uuid'])
            Composition.objects.create(module=module, composant=composant, quantite=composant_associe['quantite'])

        return module
