from rest_framework import viewsets, status
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView

from .services import MessageService, ImportService
from .serializers import GammeSerializer, FileSerializer, ModuleImporteSerializer, ModuleSerializer, ComposantSerializer
from .models import Gamme, Module, Composant


class GammeViewSet(viewsets.ModelViewSet):
    queryset = Gamme.objects.all()
    serializer_class = GammeSerializer
    keycloak_roles = {
        'GET': ['bureau_etude', 'commercial'],
        'POST': ['bureau_etude'],
        'PUT': ['bureau_etude'],
    }

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        message_service = MessageService()
        gamme_json = JSONRenderer().render(serializer.data)
        message_service.envoyer_message('gamme.cree', gamme_json)
        return Response(serializer.data)


class ImportModuleView(APIView):
    parser_classes = (MultiPartParser, FormParser)
    keycloak_roles = {
        'POST': ['bureau_etude'],
    }

    def post(self, request, *args, **kwargs):
        file_serializer = FileSerializer(data=request.data)
        file_serializer.is_valid(raise_exception=True)
        fichier = file_serializer.save()
        import_service = ImportService()
        module = import_service.importer_module(fichier)
        serializer = ModuleImporteSerializer(module)
        module_json = JSONRenderer().render(serializer.data)
        message_service = MessageService()
        message_service.envoyer_message('module.cree', module_json)

        return Response(serializer.data, status=status.HTTP_201_CREATED)


class ModuleViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Module.objects.all()
    serializer_class = ModuleSerializer
    keycloak_roles = {
        'GET': ['bureau_etude', 'commercial'],
    }


class ComposantViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Composant.objects.all()
    serializer_class = ComposantSerializer
    keycloak_roles = {
        'GET': ['bureau_etude', 'commercial'],
    }
