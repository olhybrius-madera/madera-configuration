from rest_framework import serializers

from .models import Gamme, Module, File, Composition, Composant


class ModulesGammeSerializer(serializers.ModelSerializer):
    uuid = serializers.UUIDField()

    class Meta:
        model = Module
        fields = ('uuid',)
        read_only_fields = ('uuid',)


class GammeSerializer(serializers.ModelSerializer):
    modules = ModulesGammeSerializer(many=True, source='module_set', read_only=True)

    class Meta:
        model = Gamme
        fields = ('uuid', 'libelle', 'finition_exterieure', 'type_isolant', 'type_couverture', 'modules')
        read_only_fields = ('uuid', 'modules')


class FileSerializer(serializers.ModelSerializer):
    class Meta:
        model = File
        fields = "__all__"


class CompositionSerializer(serializers.ModelSerializer):
    uuid = serializers.UUIDField(source='composant.uuid')

    class Meta:
        model = Composition
        fields = ('uuid', 'quantite')


class ModuleImporteSerializer(serializers.ModelSerializer):
    composants = CompositionSerializer(many=True, source='composition_set')

    class Meta:
        model = Module
        fields = ('uuid', 'libelle', 'unite', 'gamme', 'composants')


class ModuleSerializer(serializers.ModelSerializer):
    composants = CompositionSerializer(many=True, source='composition_set')

    class Meta:
        model = Module
        fields = ('uuid', 'libelle', 'unite', 'gamme', 'prix', 'composants')


class ComposantSerializer(serializers.ModelSerializer):
    class Meta:
        model = Composant
        fields = '__all__'
