import decimal

from django.db import models
import uuid


class Unite(models.TextChoices):
    CM = 'CM'
    PIECE = 'PIECE'
    M2 = 'M2'
    M = 'M'


class Composant(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    libelle = models.CharField(max_length=50)
    prix = models.DecimalField(max_digits=19, decimal_places=2)
    unite = models.CharField(choices=Unite.choices, max_length=50)


class Module(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    libelle = models.CharField(max_length=50)
    unite = models.CharField(choices=Unite.choices, max_length=50)
    gamme = models.ForeignKey('Gamme', on_delete=models.CASCADE)

    @property
    def prix(self):
        composition = self.composition_set.all()
        prix = 0

        for composant in composition:
            quantite = composant.quantite
            prix_composant = composant.composant.prix

            prix += prix_composant * decimal.Decimal(quantite)

        return prix


class Composition(models.Model):
    module = models.ForeignKey(Module, on_delete=models.CASCADE)
    composant = models.ForeignKey(Composant, on_delete=models.CASCADE)
    quantite = models.FloatField()


class Gamme(models.Model):
    class TypeIsolant(models.TextChoices):
        SYNTHETIQUE = 'SYNTHETIQUE'
        NATUREL = 'NATUREL'
        BIOLOGIQUE = 'BIOLOGIQUE'

    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    libelle = models.CharField(max_length=50)
    finition_exterieure = models.CharField(max_length=50)
    type_isolant = models.CharField(choices=TypeIsolant.choices, max_length=50)
    type_couverture = models.CharField(max_length=50)


class File(models.Model):
    file = models.FileField(blank=False, null=False)

    def __str__(self):
        return self.file.name
