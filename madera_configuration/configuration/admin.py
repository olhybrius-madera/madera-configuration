from django.contrib import admin

from .models import Gamme, Module, Composition, Composant

admin.site.register(Gamme)
admin.site.register(Module)
admin.site.register(Composition)
admin.site.register(Composant)


