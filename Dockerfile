FROM python:3

ARG USER_ID
ARG GROUP_ID
ARG PROJECT_NAME

RUN addgroup --gid $GROUP_ID user
RUN adduser --disabled-password --gecos '' --uid $USER_ID --gid $GROUP_ID user

RUN mkdir /app
RUN chown user /app
USER user

ENV PATH "$PATH:/home/user/.local/bin"
ENV PYTHONUNBUFFERED=1

WORKDIR /app

COPY --chown=user . /app/
RUN pip install --user -r requirements.txt
RUN chmod +x scaffold.sh
RUN chmod +x start.sh
